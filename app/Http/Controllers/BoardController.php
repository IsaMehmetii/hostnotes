<?php

namespace App\Http\Controllers;

use App\Http\Requests\Boards\BoardStore;
use App\Http\Requests\Boards\BoardTeamChange;
use App\Services\BoardService;
use App\Services\TeamService;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class BoardController extends Controller
{
    protected $boardService;
    protected $teamService;

    public function __construct(BoardService $boardService, TeamService $teamService)
    {
        $this->boardService = $boardService;
        $this->teamService = $teamService;
    }

    public function getUserBoards()
    {
        try {
        $boards = auth()->user()->board_members->where('closed', false);
        $this->boardService->ifExists($boards);
        return response()->json($boards);
        } catch (\Exception $e) {
            return response()->json(['Boards found' => false, 'Message' => $e->getMessage()], 404);
        }
    }

    public function publicBoards()
    {
        try {
        $boards =$this->boardService->getAllPublicBoards();
        $this->boardService->ifExists($boards);
        return response()->json($boards);
        } catch (\Exception $e) {
            return response()->json(['Boards found' => false, 'Message' => $e->getMessage()], 404);
        }
    }

    public function closed()
    {
        try {
        $boards = auth()->user()->board_members->where('closed', true);
        $this->boardService->ifExists($boards);
        return response()->json($boards);
        } catch (\Exception $e) {
            return response()->json(['Boards found' => false, 'Message' => $e->getMessage()], 404);
        }

    }

    public function store(BoardStore $request)
    {
        try {
            //grab the team then add team->board
            $board = $this->teamService->getTeamById($request->team_id)
                ->boards()->create($request->all());
            $board->board_members()->attach(auth()->user(), ['role' => 'admin']);
            $board->settings()->create();
            $board->addActivity('added');
            return response()->json(['Board added' => true], 201);
        } catch (\Exception $e) {
            return response()->json(['Board added' => false, 'Message' => $e->getMessage()], 404);
        }
    }


    public function show($id)
    {
        try {
            $board = $this->boardService->getBoardById($id);
            return response()->json($board, 200);
        }catch (\Exception $e) {
            return response()->json(['Board found' => false, 'Message' => $e->getMessage()], 404);
        }
    }
    public function visibility( $id )
    {
        try {
            $board = $this->boardService->getBoardById($id);
            return response()->json($board->visibility, 200);
        }catch (\Exception $e) {
            return response()->json(['Board found' => false, 'Message' => $e->getMessage()], 404);
        }
    }
    public function editVisibility(Request $request,$id)
    {
         $this->validate($request,[
            'visibility' => [
                'required',
                Rule::in(['team', 'private', 'public', 'organization']),
            ],
        ]);
        try {
            $board = $this->boardService->getBoardById($id);
            $board->visibility = $request->visibility;
            $board->save();
            return response()->json(['Board updated' => true], 200);
        }catch (\Exception $e) {
            return response()->json(['Board found' => false, 'Message' => $e->getMessage()], 404);
        }

    }

    public function changeTeam(BoardTeamChange $request, $id)
    {

        try {
            $board = $this->boardService->getBoardById($id);
            $board->team_id = $request->team_id;
            $board->save();
            return response()->json(['Board Team updated' => true], 200);
        }catch (\Exception $e) {
            return response()->json(['Board Team updated' => false, 'Message' => $e->getMessage()], 404);
        }
    }

    public function renameBoard(Request $request, $id)
    {
        $request->validate(['name' => 'required|max:75|string']);
        try {
            $board = $this->boardService->getBoardById($id);
            $board->name = $request->name;
            $board->save();
            return response()->json(['Board Name updated' => true], 200);
        }catch (\Exception $e) {
            return response()->json(['Board Name updated' => false, 'Message' => $e->getMessage()], 404);
        }
    }

    public function destroy($id)
    {
        try {
            $board = $this->boardService->getBoardById($id);
            $board->delete();
            $board->addActivity('deleted');
            return response()->json(['Board Deleted' => true], 200);
        }catch (\Exception $e) {
            return response()->json(['Board Deleted' => false, 'Message' => $e->getMessage()], 404);
        }
    }

    public function close($id)
    {
        try {
            $board = $this->boardService->getBoardById($id);
            $board->closed = true;
            $board->save();
            $board->addActivity('closed');
            return response()->json(['Board closed' => true], 200);
        }catch (\Exception $e) {
            return response()->json(['Board closed' => false, 'Message' => $e->getMessage()], 404);
        }
    }
    public function reopen($id)
    {
        try {
            $board = $this->boardService->getBoardById($id);
            $board->closed = false;
            $board->save();
            $board->addActivity('reopened');
            return response()->json(['Board reopened' => true], 200);
        }catch (\Exception $e) {
            return response()->json(['Board reopened' => false, 'Message' => $e->getMessage()], 404);
        }
    }
}
