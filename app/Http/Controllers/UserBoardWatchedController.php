<?php

namespace App\Http\Controllers;

use App\Services\BoardService;
use App\Services\TeamService;
use Illuminate\Http\Request;

class UserBoardWatchedController extends Controller
{
    protected $boardService;
    protected $teamService;

    public function __construct(BoardService $boardService, TeamService $teamService)
    {
        $this->boardService = $boardService;
        $this->teamService = $teamService;
    }

    public function watchBoard($id)
    {
        try {
            $board = $this->boardService->getBoardById($id);
            if($board->user_watched()->newPivotStatementForId(auth()->user()->id)->exists()) {
                throw new \Exception('User Already watched board');
            }
            $board->user_watched()->attach(auth()->user()->id);
            return response()->json(['Boards watched' => true], 201);
        } catch (\Exception $e) {
            return response()->json(['Boards watched' => false, 'Message' => $e->getMessage()], 404);
        }
    }

    public function removeWatchBoard($id)
    {
        try {
            $board = $this->boardService->getBoardById($id);
            if(!$board->user_watched()->newPivotStatementForId(auth()->user()->id)->exists()) {
                throw new \Exception('User hasn\'t watched board');
            }
            $board->user_watched()->detach(auth()->user()->id);
            return response()->json(['Boards watched remove' => true], 201);
        } catch (\Exception $e) {
            return response()->json(['Boards watched remove' => false, 'Message' => $e->getMessage()], 404);
        }
    }
}
