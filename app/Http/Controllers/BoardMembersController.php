<?php

namespace App\Http\Controllers;

use App\Http\Requests\Boards\BoardMember;
use App\Notifications\InvitesBoardMember;
use App\Services\BoardService;
use App\Services\TeamService;
use App\User;

class BoardMembersController extends Controller
{
    protected $boardService;
    protected $teamService;
    public function __construct(BoardService $boardService, TeamService $teamService)
    {
        $this->boardService = $boardService;
        $this->teamService = $teamService;
    }

//    public function memberBoards()
//    {
//        try {
//            $teams = auth()->user()->team_members;
//            $this->teamService->checkIfTeamsExist($teams);
//            return response()->json($teams);
//        } catch (\Exception $e) {
//            return response()->json(['Message' => $e->getMessage()],404);
//        }
//    }
    public function boardMembers($id)
    {
        try {
            $board = $this->boardService->getBoardById($id);
            $this->boardService->ifExists($board->board_members);
            return response()->json($board->board_members);
        } catch (\Exception $e) {
            return response()->json(['Members found' => false, 'Message' => $e->getMessage()],404);
        }
    }
    public function boardMembersCount($id)
    {
        try {
            $board = $this->boardService->getBoardById($id);
            abort_if ($board->board_members->count() == 0, '404', 'No member found');
            return response()->json($board->board_members->count(), 200);
        } catch (\Exception $e) {
            return response()->json(['Members found' => false, 'Message' => $e->getMessage()],404);
        }
    }

    public function inviteBoardMember(BoardMember $request, $id)
    {
        try {
        $board = $this->boardService->getBoardById($id);

        //abort if user exists
            $user = User::findOrFail($request->user_id);
            if($board->hasMember($user)){
                $this->boardService->exception('User Exists');
            }
            $board->board_members()->attach($request->user_id, ['role' => 'user']);
            $user->notify(new InvitesBoardMember([auth()->user(), $board]));
            $board->addActivity('invited', $user->name.' to');
            return response()->json(['User invited' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['User Invited' => false, 'Message' => $e->getMessage()],404);
        }
    }

    public function joinBoard($id)
    {
        try {
            $board = $this->boardService->getBoardById($id);
            if ($board->hasVisibility('team')){
                if($board->hasTeamMember(auth()->user()) and !$board->hasMember(auth()->user())){
                    $board->board_members()->attach(auth()->user(), ['role' => 'user']);
                    return response()->json(['User joined' => true], 200);
                }
            }
            $this->boardService->exception('Unauthorized');
        } catch (\Exception $e) {
            return response()->json(['User joined' => false, 'Message' => $e->getMessage()],404);
        }
    }
    public function boardMemberLeave($id)
    {
        try {
            $board = $this->boardService->getBoardById($id);
            $board->board_members()->detach(auth()->user());
            $board->addActivity('left', 'from');
            return response()->json(['User left' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['User left' => false, 'Message' => $e->getMessage()],404);
        }
    }

    public function memberRemove($board_id, $user_id)
    {
        try {
            $board = $this->boardService->getBoardById($board_id);
            $user = User::findOrFail($user_id);
            if($user == auth()->user()){
                throw new \Exception('You can\'t remove yourself', 403);
            }
            $this->boardService->detachMemberFromBoard($user, $board);
            $board->addActivity('removed', $user->name.'from');
            return response()->json(['Member removed' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['Member removed' => false, 'Message' => $e->getMessage()],404);
        }
    }

}
