<?php

namespace App\Http\Controllers;

use App\Models\Board_task;
use App\Services\BoardListService;
use App\Services\BoardService;
use Illuminate\Http\Request;

class BoardTaskController extends Controller
{
    protected $boardService;
    protected $teamService;
    public function __construct(BoardService $boardService, BoardListService $teamService)
    {
        $this->boardService = $boardService;
        $this->teamService = $teamService;
    }

    public function index($board_id, $list_id)
    {
        try {
           $board = $this->boardService->getBoardById($board_id);
           $list = $board->lists()->where('id', $list_id)->firstOrFail();
           $this->boardService->ifExists($list->tasks);
           return response()->json($list->tasks, 200);
        }catch (\Exception $e) {
            return response()->json(['Board task found' => false, 'Message' => $e->getMessage()], 404);
        }
    }
     public function show($board_id, $list_id, $task_id)
    {
         try {
           $board = $this->boardService->getBoardById($board_id);
           $list = $board->lists()->where('id', $list_id)->firstOrFail();
           $this->boardService->ifExists($list->tasks->where('id', $task_id));
           return response()->json($list->tasks->where('id', $task_id), 200);
        }catch (\Exception $e) {
            return response()->json(['Board task found' => false, 'Message' => $e->getMessage()], 404);
        }
    }

    public function count($board_id, $list_id)
    {
        try {
            $board = $this->boardService->getBoardById($board_id);
             $list = $board->lists()->where('id', $list_id)->firstOrFail();
            return response()->json($list->tasks()->count(), 200);
        }catch (\Exception $e) {
            return response()->json(['Board List stored' => false, 'Message' => $e->getMessage()], 404);
        }
    }

    public function store($board_id, $list_id, Request $request)
    {
         try {
           $board = $this->boardService->getBoardById($board_id);
           $list = $board->lists()->where('id', $list_id)->firstOrFail();
           $list->tasks()->create([
                'description' => $request->description,
                'name' => $request->name,
           ]);
            return response()->json(['Board Task stored' => true], 200);
        }catch (\Exception $e) {
            return response()->json(['Board Task stored' => false, 'Message' => $e->getMessage()], 404);
        }
    }
//    public function update($board_id, $list_id, BoardListUpdateRequest $request)
//    {
//         try {
//            $board = $this->boardService->getBoardById($board_id);
//            $list = $board->lists()->where('id', $list_id)->firstOrFail();
//            $list->update($request->all());
//            return response()->json(['Board List updated' => true], 200);
//        }catch (\Exception $e) {
//            return response()->json(['Board List updated' => false, 'Message' => $e->getMessage()], 404);
//        }
//    }
}
