<?php

namespace App\Http\Controllers;

use App\Models\User_detail;
use App\Models\User_setting;
use CURLFile;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function sessionIndex()
    {
        return response()->json(auth()->user()->session);
    }
    public function detailsIndex()
    {
        return response()->json(auth()->user()->detail);
    }
    public function settingsIndex()
    {
        return response()->json(auth()->user()->setting);
    }
    public function details(Request $request)
    {
        $data = $this->validate($request, [
        'username' => 'sometimes|max:150|min:3',
        'bio' => 'sometimes|max:150|min:3',
        'job_title' => 'sometimes|max:150|min:3'
    ]);

        try {
            $details = auth()->user()->detail;
            if ($request->username != ""){
            $details->username = $request->username;
            }
            if ($request->bio != "") {
                $details->bio = $request->bio;
            }
            if ($request->job_title != ""){
                $details->job_title = $request->job_title;
            }
            if ($request->company != ""){
                $details->company = $request->company;
            }if ($request->phone_number != ""){
                $details->phone_number = $request->phone_number;
            }
            $details->save();

            return response()->json(['Profile updated' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['Profile updated' => false, 'Message' => $e->getMessage()], 404);
        }
;    }

    public function settings(Request $request)
    {

         $this->validate($request, [
        'suggestions' => 'required|boolean',
        'accessibility' => 'required|boolean',
        'privacy' => 'required|boolean'
        ]);

        try {
            $settings = auth()->user()->setting;
            $settings->suggestions = $request->suggestions;
            $settings->accessibility = $request->accessibility;
            $settings->privacy = $request->privacy;
            $settings->save();

            return response()->json(['Profile updated' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['Profile updated' => false, 'Message' => $e->getMessage()], 404);
        }
    }


    public function postProfileImage(Request $request)
    {
        if($request->hasFile('file')){
            $file = $request->file('file');
            $extension=$file->extension();
            $cfile= new CURLFile($_FILES['file']['tmp_name'],$_FILES['file']['type'],$_FILES['file']['name']);
            $data1 = [
                'file' => $cfile,
                'filename' => uniqid().'.'.$extension,
                'directory'=> 'uploads/user/image/'
            ];
            $url = 'http://localhost:8012/upload.php';
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30000,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => json_encode($data1),
                CURLOPT_HTTPHEADER => array(
                    // Set here requred headers
                    "accept: */*",
                    "accept-language: en-US,en;q=0.8",
                    "content-type: application/json",
                ),
            ));
            $response = curl_exec($curl);

            curl_close($curl);
            return response()->json($response);
        }else{
            return response()->json('No file');
        }
    }



//    public function getProfileImage()
//    {
//        $data1 = [
//            'uid'=> '601d61ded4218',
//            'mime'=> 'jpg',
//            'directory'=> '/uploads/user/image/',
//        ];
//
//        $query = http_build_query($data1);
////        dd($query);
//        $curl = curl_init();
//        curl_setopt_array($curl, array(
//            CURLOPT_URL => "http://127.0.0.1:8012/download.php?".$query,
//            CURLOPT_RETURNTRANSFER => true,
//            CURLOPT_ENCODING => "",
//            CURLOPT_TIMEOUT => 30000,
//            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
//            CURLOPT_CUSTOMREQUEST => "GET",
//            CURLOPT_HTTPHEADER => array(
////                 Set Here Your Requested Headers
//                'Content-Type: application/json',
//            ),
//        ));
//        $response = curl_exec($curl);
////        dd($response);
//        $err = curl_error($curl);
//        curl_close($curl);
//        if ($err) {
//            echo "cURL Error #:" . $err;
//        } else {
//            return response()->json(($response));
//        }
//
//    }

}
