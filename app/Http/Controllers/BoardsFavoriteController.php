<?php

namespace App\Http\Controllers;

use App\Services\BoardService;
use App\Services\TeamService;

class BoardsFavoriteController extends Controller
{
    protected $boardService;
    protected $teamService;
    public function __construct(BoardService $boardService, TeamService $teamService)
    {
        $this->boardService = $boardService;
        $this->teamService = $teamService;
    }

    public function index()
    {
        try {
            abort_if(auth()->user()->boards_favorite == '[]', 404, 'No board Found');
            return response()->json(auth()->user()->boards_favorite);
        }catch (\Exception $e) {
            return response()->json(['Boards Exist' => false, 'Message' => $e->getMessage()],400);
        }
    }

    public function store($id)
    {
        try {
            abort_if(auth()->user()->boards_favorite->contains($id), 400, 'Board already favorited');
            auth()->user()->boards_favorite()->attach($this->boardService->getBoardById($id));
            return response()->json(['Board favorited' => true], 201);
        }catch (\Exception $e) {
            return response()->json(['Board favorited' => false, 'Message' => $e->getMessage()],400);
        }
    }
    public function destroy($id)
    {
        try {
            abort_if(!auth()->user()->boards_favorite->contains($id), 400, 'Board is not favorited');
            auth()->user()->boards_favorite()->detach($this->boardService->getBoardById($id));
            return response()->json(['Board unfavorited' => true], 201);
        }catch (\Exception $e) {
            return response()->json(['Board unfavorited' => false, 'Message' => $e->getMessage()],400);
        }
    }
}
