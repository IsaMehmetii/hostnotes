<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Services\TeamService;
use Illuminate\Http\Request;

class TeamActivitiesController extends Controller
{
    protected $teamService;
    public function __construct(TeamService $teamService)
    {
        $this->teamService = $teamService;
    }

    public function activitiesIndex($id)
    {
        try {
            $team = Team::findOrFail($id);

            return response()->json($team->activities);
        }catch (\Exception $e) {
            return response()->json(['Activities found' => false, 'Message' => $e->getMessage()], 404);
        }
    }
    public function activitiesShow($team_id, $activity_id)
    {
        try {
            $team = Team::findOrFail($team_id);
            if(!auth()->user()->hasTeamPermissionTo('view-team', $team_id )){
                throw new \Exception('Unauthorized.', 403);
            }
            if(!$activity = $team->activities()->where('id', $activity_id)->first()){
                throw new \Exception('No Activities found with id: '.$activity_id, 404);
            }
            return response()->json($activity);
        }catch (\Exception $e) {
            return response()->json(['Activities found' => false, 'Message' => $e->getMessage()], 404);
        }
    }
}
