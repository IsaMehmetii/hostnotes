<?php

namespace App\Http\Controllers;

use App\Services\BoardListService;
use App\Services\BoardService;

class BoardListWatchedController extends Controller
{
    protected $boardService;
    protected $teamService;
    public function __construct(BoardService $boardService, BoardListService $teamService)
    {
        $this->boardService = $boardService;
        $this->teamService = $teamService;
    }

     public function watchList($board_id, $list_id)
    {
        try {
            $board = $this->boardService->getBoardById($board_id);
            $list = $board->lists()->where('id', $list_id)->firstOrFail();

            if($list->user_watched()->newPivotStatementForId(auth()->user()->id)->exists()) {
                throw new \Exception('User Already watched board list');
            }
            $list->user_watched()->attach(auth()->user()->id);
            return response()->json(['Board list watched' => true], 201);
        } catch (\Exception $e) {
            return response()->json(['Board list watched' => false, 'Message' => $e->getMessage()], 404);
        }
    }

    public function removeWatchList($board_id, $list_id)
    {
        try {
            $board = $this->boardService->getBoardById($board_id);
            $list = $board->lists()->where('id', $list_id)->firstOrFail();
            if(!$list->user_watched()->newPivotStatementForId(auth()->user()->id)->exists()) {
                throw new \Exception('User Already watched board list');
            }
            $list->user_watched()->detach(auth()->user()->id);
            return response()->json(['Board list watched remove' => true], 201);
        } catch (\Exception $e) {
            return response()->json(['Board list watched remove' => false, 'Message' => $e->getMessage()], 404);
        }
    }
}
