<?php

namespace App\Http\Controllers;

use App\Services\BoardService;
use App\Services\TeamService;
use Illuminate\Http\Request;

class BoardActivitiesController extends Controller
{
    protected $boardService;
    protected $teamService;

    public function __construct(BoardService $boardService, TeamService $teamService)
    {
        $this->boardService = $boardService;
        $this->teamService = $teamService;
    }

    public function index($id)
    {
        try {
            $board = $this->boardService->getBoardById($id);
            return response()->json($board->activities, 200);
        }catch (\Exception $e) {
            return response()->json(['Board activity found' => false, 'Message' => $e->getMessage()], 404);
        }
    }
    public function show($id, $activity_id)
    {
         try {
            $board = $this->boardService->getBoardById($id);
            return response()->json($board->activities->where('id', $activity_id)->first(), 200);
        }catch (\Exception $e) {
            return response()->json(['Board activity found' => false, 'Message' => $e->getMessage()], 404);
        }
    }
}
