<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserNotificationsController extends Controller
{
    public function index()
    {
        return response()->json(auth()->user()->notifications);
    }
    public function show($id)
    {
        if(!$notification = auth()->user()->notifications->where('id', $id)->first()){
            return response()->json(['Notification found' => false], 404);
        }
        return response()->json($notification);
    }
    public function showAndMarkRead($id)
    {
        if(!$notification = auth()->user()->notifications->where('id', $id)->first()){
            return response()->json(['Notification found' => false], 404);
        }
        $notification->markAsRead();
        return response()->json($notification);
    }
    public function readNotifications()
    {
        try {
            $notifications = auth()->user()->readNotifications;
            $this->checkIfNotificationsExist($notifications);
            return response()->json($notifications);
        }  catch (\Exception $e) {
            return response()->json(['Notifications found' => false, 'Message' => $e->getMessage()], 404);
        }

    }
    public function unreadNotifications()
    {
        try {
            $notifications = auth()->user()->unreadNotifications;
            $this->checkIfNotificationsExist($notifications);
            return response()->json($notifications);
        }  catch (\Exception $e) {
            return response()->json(['Notifications found' => false, 'Message' => $e->getMessage()], 404);
        }
    }
    public function countUnread()
    {
        try {
            return response()->json(auth()->user()->unreadNotifications->count());
        }  catch (\Exception $e) {
            return response()->json(['Notifications found' => false, 'Message' => $e->getMessage()], 404);
        }
    }

    public function markAllAsRead()
    {
        try {
            $notifications = auth()->user()->unreadNotifications;
            $this->checkIfNotificationsExist($notifications);
            foreach ($notifications as $notification){
                $notification->markAsRead();
            }
            return response()->json([$notifications->count().' Notifications updated' => true], 200);
        }  catch (\Exception $e) {
            return response()->json(['Notifications updated' => false, 'Message' => $e->getMessage()], 404);
        }
    }
    public function markAsReadById($id)
    {
        if(!$notification = auth()->user()->notifications->where('id', $id)->first()){
            return response()->json(['Notification found' => false], 404);
        }
        $notification->markAsRead();
        return response()->json(['Notification updated' => true], 200);
    }
    public function markAsUnReadById($id)
    {
        if(!$notification = auth()->user()->notifications->where('id', $id)->first()){
            return response()->json(['Notification found' => false], 404);
        }
        $notification->markAsUnread();
        return response()->json(['Notification updated' => true], 200);
    }

    public function checkIfNotificationsExist($notifications)
    {
        if ($notifications== '[]'){
            throw new \Exception('No notification was found.');
        }
    }
}
