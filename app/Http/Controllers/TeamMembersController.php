<?php

namespace App\Http\Controllers;

use App\Http\Requests\TeamRequests\TeamMembersInvite;
use App\Models\Team;
use App\Models\Team_role;
use App\Notifications\InvitesTeamMember;
use App\Services\TeamService;
use App\User;
use Illuminate\Http\Request;

class TeamMembersController extends Controller
{
    protected $teamService;
    public function __construct(TeamService $teamService)
    {
        $this->teamService = $teamService;
    }

    public function memberIndex()
    {
        try {
            $teams = auth()->user()->team_members;
            $this->teamService->checkIfTeamsExist($teams);
            return response()->json($teams);
        } catch (\Exception $e) {
            return response()->json(['Message' => $e->getMessage()],404);
        }
    }

    public function teamMembers($id)
    {
        try {

            $team = Team::findOrFail($id);
            if ($this->teamService->checkMember($team)){
                throw new \Exception('Unauthorized',403);
            }
            $this->teamService->checkIfMembersExist($team->team_members);
            foreach ($team->team_members as $member){
                $member->team_roles->first();
            }
            return response()->json($team->team_members);
        } catch (\Exception $e) {
            return response()->json(['Message' => $e->getMessage()],404);
        }
    }
    public function teamMemberCount($id)
    {
        try {
            $team = Team::findOrFail($id);
            abort_if ($team->team_members->count() == 0, '404', 'No member found');
            return response()->json($team->team_members->count());
        } catch (\Exception $e) {
            return response()->json(['Message' => $e->getMessage()],404);
        }
    }
    public function inviteTeamMembers(TeamMembersInvite $request, $id)
    {
        try {
            $team = Team::findOrFail($id);
            $role = Team_role::findOrFail($request->role_id);
            if ($this->teamService->checkMember($team)){
                throw new \Exception('Unauthorized',403);
            }
            foreach ($request->user_id as $user_id){
                $this->teamService->checkUserMemberExist($team, $user_id);
                $user = User::findOrFail($user_id);
                $team->team_members()->attach($user_id);
                $user->giveTeamRolesTo([$role->slug], $team->id);
                foreach ($role->permissions as $permission){
                    $user->giveTeamPermissionsTo([$permission->slug], $team->id);
                }
                $user->notify(new InvitesTeamMember([auth()->user(), $team]));
            }
            $team->addActivity('invited', $user->name.' to');
            return response()->json(['User invited' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['User invited' => false, 'Message' => $e->getMessage()], 404);
        }
    }

    public function memberLeave($id)
    {
        try{
            if(!auth()->user()->hasTeamPermissionTo('remove-member', $id )){
                throw new \Exception('Unauthorized.', 403);
            }
            $team = Team::findOrFail($id);

            $user = auth()->user();
            if (!$team->team_members->contains($user->id)){
                throw new \Exception('Member doesn\'t exist');
            }
            $this->teamService->detachMemberFromTeam($user, $team);
            //if the last team member leaves the team gets deleted
            if ($team->team_members->count() == 1){
                $team->addActivity('deleted');
                $team->delete();
                return response()->json(['Member leave' => true, 'Team Deleted' => true], 200);
            }
            return response()->json(['Member leave' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['Member leave' => false, 'Message' => $e->getMessage()],404);
        }
    }

    public function memberRemove($team_id, $user_id)
    {
        try {
            $team = Team::findOrFail($team_id);
            $user = User::findOrFail($user_id);
            if($user == auth()->user()){
                throw new \Exception('You can\'t remove yourself', 403);
            }
            $this->teamService->detachMemberFromTeam($user, $team);
            return response()->json(['Member removed' => true], 200);
        } catch (\Exception $e) {
            return response()->json(['Member removed' => false, 'Message' => $e->getMessage()],404);
        }
    }
}
