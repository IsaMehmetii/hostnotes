<?php

namespace App\Http\Controllers;

use App\Http\Requests\Boards\BoardListStoreRequest;
use App\Http\Requests\Boards\BoardListUpdateRequest;
use App\Services\BoardListService;
use App\Services\BoardService;
use Illuminate\Http\Request;

class BoardListController extends Controller
{
    protected $boardService;
    protected $teamService;
    public function __construct(BoardService $boardService, BoardListService $teamService)
    {
        $this->boardService = $boardService;
        $this->teamService = $teamService;
    }

    public function index($board_id)
    {
        try {
            $board = $this->boardService->getBoardById($board_id);
            $this->boardService->ifExists($board->lists->where('archived', false));
            return response()->json($board->lists->where('archived', false), 200);
        }catch (\Exception $e) {
            return response()->json(['Board List stored' => false, 'Message' => $e->getMessage()], 404);
        }
    }
     public function show($board_id, $list_id)
    {
         try {
            $board = $this->boardService->getBoardById($board_id);
            $list = $board->lists()->where('id', $list_id)->firstOrFail();
            return response()->json($list, 200);
        }catch (\Exception $e) {
            return response()->json(['Board List found' => false, 'Message' => $e->getMessage()], 404);
        }
    }

    public function count($board_id)
    {
        try {
            $board = $this->boardService->getBoardById($board_id);
            return response()->json($board->lists->where('archived', false)->count(), 200);
        }catch (\Exception $e) {
            return response()->json(['Board List stored' => false, 'Message' => $e->getMessage()], 404);
        }
    }

    public function store($board_id, BoardListStoreRequest $request)
    {
         try {
            $board = $this->boardService->getBoardById($board_id);
            $board->lists()->create($request->all());
            return response()->json(['Board List stored' => true], 200);
        }catch (\Exception $e) {
            return response()->json(['Board List stored' => false, 'Message' => $e->getMessage()], 404);
        }
    }
    public function update($board_id, $list_id, BoardListUpdateRequest $request)
    {
         try {
            $board = $this->boardService->getBoardById($board_id);
            $list = $board->lists()->where('id', $list_id)->firstOrFail();
            $list->update($request->all());
            return response()->json(['Board List updated' => true], 200);
        }catch (\Exception $e) {
            return response()->json(['Board List updated' => false, 'Message' => $e->getMessage()], 404);
        }
    }

    public function destroy($board_id, $list_id)
    {
         try {
            $board = $this->boardService->getBoardById($board_id);
            $board->lists()->where('id', $list_id)->delete();
            return response()->json(['Board List deleted' => true], 200);
        }catch (\Exception $e) {
            return response()->json(['Board List deleted' => false, 'Message' => $e->getMessage()], 404);
        }
    }
    public function archive($board_id, $list_id)
    {
         try {
            $board = $this->boardService->getBoardById($board_id);
            $board->lists()->where('id', $list_id)->update([
                'archived' => true
            ]);
            return response()->json(['Board List archived' => true], 200);
        }catch (\Exception $e) {
            return response()->json(['Board List archived' => false, 'Message' => $e->getMessage()], 404);
        }
    }
    public function restore($board_id, $list_id)
    {
         try {
            $board = $this->boardService->getBoardById($board_id);
            $board->lists()->where('id', $list_id)->update([
                'archived' => false
            ]);
            return response()->json(['Board List restored' => true], 200);
        }catch (\Exception $e) {
            return response()->json(['Board List restored' => false, 'Message' => $e->getMessage()], 404);
        }
    }

    public function move($board_id, $list_id, Request $request)
    {
          try {
            $board = $this->boardService->getBoardById($board_id);
            $board->lists()->where('id', $list_id)->update([
                'position' => $request->position,
                'board_id' => $request->board_id,
            ]);
            return response()->json(['Board List moved' => true], 200);
        }catch (\Exception $e) {
            return response()->json(['Board List moved' => false, 'Message' => $e->getMessage()], 404);
        }
    }
}
