<?php

namespace App\Http\Controllers;

use App\Services\TeamService;
use Illuminate\Http\Request;

class TeamRolePermissionsController extends Controller
{
    protected $teamService;
    public function __construct(TeamService $teamService)
    {
        $this->teamService = $teamService;
    }
    public function roleIndex()
    {
        return response()->json($this->teamService->getAllTeamRoles());
    }
    public function permissionIndex()
    {
        return response()->json($this->teamService->getAllTeamPermissions());
    }
}
