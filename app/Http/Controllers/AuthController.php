<?php

namespace App\Http\Controllers;

use App\Models\User_activity;
use App\Models\User_detail;
use App\Models\User_session;
use App\Models\User_setting;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Mobile_Detect;
use Carbon\Carbon;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    public function register(Request $request)
    {
        $data = $this->validate($request,[
            'name' => 'required|max:75|string|min:3',
            'lastname' => 'required|max:75|string|min:3',
            'email' => 'required|max:75|string|unique:users|email',
            'password' => ['required', 'min:6', 'confirmed']
        ]);
        $details = $this->validate($request,[
            'company' => 'required|max:150|string',
            'phone_number' => 'required|max:150',
        ]);

        try {
            $data['password'] = app('hash')->make($data['password']);
            $user = User::create($data);
            $user->detail()->create($details);
            $user->setting()->create();
            $user->activity()->create();

            if ($user->save()) {
                $dt = new Carbon();
                $session = new User_session;
                $detect = new Mobile_Detect();
                if ($detect->isMobile()) {
                    $session->logged_device = 'mobile';
                } else {
                    $session->logged_device = 'desktop';
                }
                $session->user_agent = $request->server('HTTP_USER_AGENT');
                $session->user_ip = $request->ip();
                $session->last_activity = $dt->isoFormat('YYYY-MM-DD HH:mm:ss');
                $user->session()->save($session);
            }
            $token = auth()->attempt(request(['email', 'password']));
            return response()->json(['User added' => true, 'token' => $token], 201);

        } catch (\Exception $e) {
            return response()->json(['User added' => false,'message' => $e->getMessage()], $e->getCode());
        }
    }
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $session = auth()->userOrFail()->session;
        $dt = new Carbon();
        $detect = new Mobile_Detect();
        if ( $detect->isMobile() ) {
            $session->logged_device = 'mobile';
        } else {
            $session->logged_device = 'desktop';
        }
        $session->user_agent = $request->server('HTTP_USER_AGENT');
        $session->user_ip = $request->ip();
        $session->last_activity = $dt->isoFormat('YYYY-MM-DD HH:mm:ss');
        $session->save();

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
