<?php /** @noinspection PhpPossiblePolymorphicInvocationInspection */

namespace App\Http\Controllers;

use App\Http\Requests\Team\TeamSettingsUpdate;
use App\Http\Requests\TeamRequests\TeamUpdate;
use App\Http\Requests\TeamRequests\TeamStore;
use App\Models\Team;
use App\Models\Team_role;
use App\Services\TeamService;

class TeamController extends Controller
{
    protected $teamService;
    public function __construct(TeamService $teamService)
    {
        $this->teamService = $teamService;
    }
    public function types()
    {
        return response()->json($this->teamService->getAllTeamTypes());
    }
    public function show($id)
    {
        try {
            $team = Team::findOrFail($id);
            if(!auth()->user()->hasTeamPermissionTo('view-team', $id )){
                throw new \Exception('Unauthorized.', 403);
            }
            return response()->json($team);
        } catch (\Exception $e) {
            return response()->json(['Message' => $e->getMessage()],404);
        }
    }
    public function visibleTeamsIndex()
    {
        try {
            $teams = $this->teamService->getVisibleTeams();
            $this->teamService->checkIfTeamsExist($teams);
            return response()->json($teams);
        } catch (\Exception $e) {
            return response()->json(['Message' => $e->getMessage()],404);
        }
    }
    public function store(TeamStore $request)
    {
        try {
            $team = Team::create($request->all());
            $team->detail()->create();
            $team->settings()->create();
            $team->team_members()->attach(auth()->user());
            $role = Team_role::findOrFail(1);
            auth()->user()->giveTeamRolesTo([$role->slug], $team->id);
            foreach ($role->permissions as $permission){
                auth()->user()->giveTeamPermissionsTo([$permission->slug], $team->id);
            }
            $team->addActivity('added');
            return response()->json(['Team added' => true], 201);
        } catch (\Exception $e) {
            return response()->json(['Team added' => false, 'Message' => $e->getMessage()],404);
        }
    }
    public function update(TeamUpdate $request, $id)
    {
            $team = Team::findOrFail($id);
        try {
            if(!auth()->user()->hasTeamPermissionTo('edit-team', $id )){
                throw new \Exception('Unauthorized.', 403);
            }
            $team->update($request->all());
            $team->addActivity('updated');

            return response()->json(['Team updated' => true]);
        } catch (\Exception $e) {
            return response()->json(['Team updated' => false, 'Message' => $e->getMessage()],404);
        }
    }

    public function settingsUpdate(TeamSettingsUpdate $request, $id)
    {
        $team = Team::findOrFail($id);
        try {
            if(!auth()->user()->hasTeamPermissionTo('edit-team', $id )){
                throw new \Exception('Unauthorized.', 403);
            }
            $team->settings()->update($request->all());
            $team->addActivity('settings updated');

            return response()->json(['Team updated' => true]);
        } catch (\Exception $e) {
            return response()->json(['Team updated' => false, 'Message' => $e->getMessage()],404);
        }

    }
    public function destroy($id)
    {
        try {
            $team = Team::findOrFail($id);
            if(!auth()->user()->hasTeamPermissionTo('delete-team', $id )){
                throw new \Exception('Unauthorized.', 403);
            }
            $this->teamService->notifyTeamMembers($team);
            $team->addActivity('deleted');
            $team->delete();
            return response()->json(['Team Deleted' => true], 200);

        }
        catch (\Exception $e) {
            return response()->json(['Team Deleted' => false, 'Message' => $e->getMessage()],404);
        }

    }

    public function teamBoards($id)
    {
        try {
            abort_if(Team::findorFail($id)->boards == '[]', 404, 'No board Found');
            return response()->json(Team::findorFail($id)->boards, 202);
        }
        catch (\Exception $e) {
            return response()->json(['Boards Exist' => false, 'Message' => $e->getMessage()],404);
        }
    }
    public function test()
    {
        $team = Team::find(1);
//        $role = Team_role::find(1);
        $idd = 4;
//        auth()->user()->notify(new InvitesTeamMember([auth()->user(), $team]));
//        dd(auth()->user()->notifications);

//        $permission = Team_permission::find(2);
//        auth()->user()->giveTeamRolesTo([$role->slug], 1);
//        foreach ($role->permissions as $permission){
//            auth()->user()->giveTeamPermissionsTo([$permission->slug], 1);
//        }

//        dd($permission);
//        $role->permissions()->attach($permission);
//        $team->addActivity('added');
//        $user = auth()->user();
//        dd($user->team_roles->first());
//        $user->team_permissions()->sync([1], ['team_id' => '1']);
//     t   $user->team_roles()->attach(1, ['team_id' => '1']);
//        $user->team_roles->where('pivot.team_id', '1'))
//        $team = Team::find(1);
        dd(auth()->user()->boards_favorite->toArray());
//        return response()->json( auth()->user()->notifications->first()->data );
//
//        $user = User::find(2);
//        dd($user->hasTeamPermissionTo('create-team', '1'));
//        dd($user->hasTeamRole(['admin'], 1));
//        dd($user->giveTeamPermissionsTo(['edit-team'], 1));
//        dd($user->withdrawTeamPermissionsTo(['edit-team']));
//        dd($user->refreshTeamPermissions(['edit-team'], '1'));
//         return $this->teamService->test();
    }


}
