<?php

namespace App\Http\Requests\Team;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class TeamSettingsUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'visibility' => [
                'required',
                Rule::in(['visible', 'private']),
            ],
        ];
    }
    public function messages(){
        return[
            'visibility.*'=>"visibility must be 'visible' or 'private'",
        ];
    }
}
