<?php

namespace App\Http\Requests\Boards;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BoardStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:75|string',
            'description' => 'required|max:150|string',
            'team_id' => 'required|exists:teams,id',
            'visibility' => [
                'required',
                Rule::in(['team', 'private', 'public', 'organization']),
            ],

        ];
    }
    public function messages(){
        return[
            'team_id.exists'=>'Team doesn\'t exist.',
            'visibility.*'=>"visibility must be 'team', 'private', 'public', 'organization'.",
        ];
    }
}
