<?php

namespace App\Http\Requests\TeamRequests;

use Illuminate\Foundation\Http\FormRequest;

class TeamStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:75|string',
            'description' => 'required|max:150|string',
            'type_id' => 'required|exists:team_types,id'
        ];
    }
    public function messages(){
        return[
            'type_id.exists'=>'Team type doesn\'t exist.',
        ];
    }
}
