<?php

namespace App\Http\Requests\TeamRequests;

use Illuminate\Foundation\Http\FormRequest;

class TeamMembersInvite extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id.*' => 'required|exists:users,id',
            'role_id' => 'required|exists:team_roles,id'
        ];

    }
    public function messages(){
        return[
            'user_id.*.exists'=>'User doesn\'t exist.',
            'user_id.*.required'=>'User is required.',
            'role_id.exists'=>'Role doesn\'t exist.',
            'role_id.required' => 'Role is required'
        ];
    }
}
