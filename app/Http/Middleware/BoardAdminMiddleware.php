<?php

namespace App\Http\Middleware;

use App\Models\Board;
use Closure;

class BoardAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //Get Board by route id and check if member is admin
        $board = Board::findOrFail($request->route('id'));
        if($board->hasAdmin(auth()->user())){
//            if ($board->isClosed()){
//                return response()->json('Board is closed, do u want to reopen it?',400);
//            }
            return $next($request);
        }
        return response()->json('Unauthorized',401);

        /*another way of doing this*/
//        auth()->user()->board_members->where('pivot.board_id', $request->route('id')
//            ->contains('pivot.role', 'admin'));
    }
}
