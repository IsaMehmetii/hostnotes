<?php

namespace App\Http\Middleware;

use App\Models\Board;
use Closure;

class BoardMemberMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $board = Board::findOrFail($request->route('id'));
        if($board->hasMember(auth()->user())){
//            if ($board->isClosed()){
//                return response()->json('Board is closed',400);
//            }
            return $next($request);
        }
        return response()->json('Unauthorized',401);
    }
}
