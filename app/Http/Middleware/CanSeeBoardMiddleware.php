<?php

namespace App\Http\Middleware;

use App\Models\Board;
use Closure;

class CanSeeBoardMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //this board user can see board and join board
        $board = Board::findOrFail($request->route('id'));
            if ($board->isClosed()){
                return response()->json('Board is closed',400);
            }
        if ($board->hasVisibility('public')){
            return $next($request);
        }else if ($board->hasVisibility('team')){
            if($board->hasMember(auth()->user()) or $board->hasTeamMember(auth()->user())){
                return $next($request);
            }
        }elseif ($board->hasVisibility('private')) {
            if ($board->hasMember(auth()->user())) {
                return $next($request);
            }
        }
        return response()->json('Unauthorized',401);
    }
}
