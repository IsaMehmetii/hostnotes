<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RemovesTeamMember extends Notification
{
    use Queueable;

    public function __construct(array $arr) {
        $this->arr = $arr;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }



    public function toDatabase($notifiable)
    {
        $user = $this->arr[0];
        $team = $this->arr[1];
        return [
            'participant' => $user->name.' '.$user->lastname,
            'url' => url('/api/'),
            'name'=> $team->name,
            'action' => 'removed',
            'title' => 'Removed From Team',
            'to_string' =>$user->name.' '.$user->lastname.' removed you from team '. $team->name,
        ];
    }




    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
