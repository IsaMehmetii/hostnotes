<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class InvitesTeamMember extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $arr;

    public function __construct(array $arr) {
        $this->arr = $arr;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }



    public function toDatabase($notifiable)
    {
        $user = $this->arr[0];
        $team = $this->arr[1];
//        dd($team);
        return [
                    'participant' => $user->name.' '.$user->lastname,
                    'url' => url('api/team/'.$team->id),
                    'name'=> $team->name,
                    'action' => 'invited',
                    'title' => 'Team Invitation',
                    'to_string' =>$user->name.' '.$user->lastname.' added you to team '. $team->name,
                ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
