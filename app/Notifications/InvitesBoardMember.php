<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class InvitesBoardMember extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    protected $arr;

    public function __construct(array $arr) {
        $this->arr = $arr;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }



    public function toDatabase($notifiable)
    {
        $user = $this->arr[0];
        $board = $this->arr[1];
        return [
                    'participant' => $user->name.' '.$user->lastname,
                    'url' => url('api/board/'.$board->id),
                    'name'=> $board->name,
                    'action' => 'invited',
                    'title' => 'Board Invitation',
                    'to_string' =>$user->name.' '.$user->lastname.' added you to board '. $board->name,
                ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
