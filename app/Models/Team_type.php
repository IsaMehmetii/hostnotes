<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team_type extends Model
{
    protected $fillable = ['type'];

    public function team()
    {
        return $this->hasOne('App\Models\Team');
    }
}
