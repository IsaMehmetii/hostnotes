<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_team_role extends Model
{
    protected $table = 'user_team_roles';
    protected $fillable = ['team_id'];

    public function team()
    {
        return $this->belongsTo('App/Models/Team');
    }
}
