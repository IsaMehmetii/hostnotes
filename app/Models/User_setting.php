<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_setting extends Model
{
    protected $fillable = [
        'user_id', 'suggestions', 'accessibility', 'privacy'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
