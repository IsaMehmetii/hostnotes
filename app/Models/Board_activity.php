<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Board_activity extends Model
{
     protected $fillable = ['user_id', 'board_id', 'action', 'addition'];

    public function user()
    {
        $this->belongsTo('App\User','user_id', 'id');
    }
    public function board()
    {
        $this->belongsTo('App\Models\board','board_id', 'id');
    }
}
