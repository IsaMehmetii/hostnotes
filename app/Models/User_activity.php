<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_activity extends Model
{
    protected $fillable = [
        'user_id', 'title', 'activity'
    ];
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
