<?php

namespace App\Models;

use App\Traits\BoardTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Board extends Model
{
    use BoardTrait, SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'visibility', 'description', 'closed', 'team_id'];

    public function team()
    {
        return $this->belongsTo('App\Models\Team');
    }
    public function board_members()
    {
        return $this->belongsToMany(
            'App\User',
            'board_members',
            'board_id',
            'user_id')->withPivot(['role']);
    }
    public function favorites()
    {
        return $this->belongsToMany(
            'App\User',
            'boards_favorite',
            'board_id',
            'user_id');
    }

    public function settings()
    {
        return $this->hasOne('App\Models\Board_setting');
    }
    public function user_watched()
    {
        return $this->belongsToMany(
            'App\Models\Board',
            'user_boards_watched',
            'board_id',
            'user_id');
    }
    public function activities()
    {
        return $this->hasMany('App\Models\Board_activity');
    }
    public function lists()
    {
        return $this->hasMany('App\Models\Board_list');
    }

}
