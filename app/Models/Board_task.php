<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Board_task extends Model
{
    protected $fillable = ['list_id', 'description', 'name', 'due_date', 'start_date',
                            'end_date', 'status', 'list_id', ];

    public function list()
    {
        return $this->belongsTo('App\Models\Board_list', 'list_id');
    }

}
