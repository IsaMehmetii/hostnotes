<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_detail extends Model
{
    protected $fillable = [
            'user_id', 'username', 'bio', 'job_title', 'company', 'phone_number',
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id', 'id');
    }
}
