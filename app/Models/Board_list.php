<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Board_list extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = [ 'name', 'position', 'board_id', 'archived'];

    public function board()
    {
        return $this->belongsTo('App\Models\Board');
    }
    public function user_watched()
    {
        return $this->belongsToMany(
            'App\Models\Board',
            'board_list_watches',
            'list_id',
            'user_id');
    }
    public function tasks()
    {
        return $this->hasMany('App\Models\Board_task', 'list_id');
    }


}
