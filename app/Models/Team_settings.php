<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team_settings extends Model
{
    protected $fillable = ['visibility'];

    public function team()
    {
        return $this->belongsTo('App\Models\Team');
    }
}
