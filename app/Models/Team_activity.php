<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team_activity extends Model
{
    protected $fillable = ['user_id', 'team_id', 'action', 'addition'];

    public function user()
    {
        $this->belongsTo('App\User','user_id', 'id');
    }
    public function team()
    {
        $this->belongsTo('App\Models\Team','team_id', 'id');
    }
}
