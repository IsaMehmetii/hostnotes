<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team_permission extends Model
{
    protected $fillable = ['name', 'slug'];

    public function roles()
    {
        return $this->belongsToMany(
            'App\Models\Team_role',
            'team_role_permissions',
            'permission_id',
            'role_id');
    }
    public function users()
    {
        return $this->belongsToMany(
            'App\User',
            'user_team_permissions',
            'permission_id',
            'user_id')
            ->withPivot(['team_id']);
    }

}
