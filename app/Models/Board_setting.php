<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Board_setting extends Model
{
    protected $fillable = ['team_members_can_edit_join'];

    public function board()
    {
        return $this->belongsTo('App\Models\Board');
    }
}
