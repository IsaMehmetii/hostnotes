<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_session extends Model
{
    protected $fillable = [
        'user_id', 'logged_device', 'user_agent', 'user_id', 'last_activity'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id', 'id');
    }
}
