<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team_detail extends Model
{
    protected $fillable = [
      'team_id', 'short_name', 'website'
    ];
    public function team()
    {
        return $this->belongsTo('App\Models\Team');
    }
}
