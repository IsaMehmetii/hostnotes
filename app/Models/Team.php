<?php

namespace App\Models;

use App\Traits\TeamTrait;
use Illuminate\Database\Eloquent\Model;
use App\Traits\TeamActivitiesTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use TeamActivitiesTrait, TeamTrait, SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'description', 'type_id',
    ];
    public function team_members()
    {
        return $this->belongsToMany(
            'App\User',
            'team_members',
            'team_id',
            'user_id');
    }
    public function detail()
    {
        return $this->hasOne('App\Models\Team_detail');
    }
    public function settings()
    {
        return $this->hasOne('App\Models\Team_settings');
    }
    public function type()
    {
        return $this->belongsTo('App\Models\Team_type');
    }
    public function role()
    {
        return $this->hasMany('App\Models\User_team_role');
    }
    public function activities()
    {
        return $this->hasMany('App\Models\Team_activity');
    }
    public function boards()
    {
        return $this->hasMany('App\Models\Board');
    }
    public function visible()
    {
        return $this->where('settings.visibility', 'visible');
    }
}

