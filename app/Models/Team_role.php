<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Team_role extends Model
{
    protected $fillable = ['name', 'slug'];

    public function permissions()
    {
        return $this->belongsToMany(
            'App\Models\Team_permission',
            'team_role_permissions',
            'role_id',
            'permission_id');
    }
    public function users()
    {
        return $this->belongsToMany(
            'App\User',
            'user_team_roles',
            'role_id',
            'user_id')
            ->withPivot(['team_id']);
    }

}
