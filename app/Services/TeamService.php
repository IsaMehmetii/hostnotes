<?php
namespace App\Services;
use App\Models\Team;
use App\Models\Team_permission;
use App\Models\Team_role;
use App\Notifications\DeletesTeam;
use App\Notifications\InvitesTeamMember;
use App\Notifications\RemovesTeamMember;
use App\User;
use App\Models\Team_type;

class TeamService{

    public function getTeamById($id)
    {
        return Team::findOrFail($id);
    }
    public function getVisibleTeams()
    {
        return Team::whereHas('settings', function ($query) {
            $query->where('visibility', '=', 'visible');
        })->get();
    }
    public function getAllTeamTypes()
    {
        return Team_type::all();
    }
    public function getAllTeamRoles()
    {
        return Team_role::all();
    }
    public function getAllTeamPermissions()
    {
        return Team_permission::all();
    }

    public function checkIfTeamsExist($teams)
    {
        if ($teams== '[]'){
            throw new \Exception('No team was found.');
        }
    }
    public function checkIfMembersExist($members)
    {
        if ($members== '[]'){
            throw new \Exception('No member was found.');
        }
    }

    public function checkMember($team)
    {
        if ($team->team_members->contains(auth()->user()->id)) {
            return false;
        }
        return true;
    }


    public function checkUserMemberExist($team, $user_id)
    {
        if($team->team_members()->newPivotStatementForId($user_id)->exists()) {
            throw new \Exception('Member exists');
        }else if ($user_id == auth()->user()->id) {
            throw new \Exception('You can\'t invite yourself');
        }
    }

    public function detachMemberFromTeam($user, $team)
    {
        $role = $user->team_roles->where('pivot.team_id', $team->id)->first();
        if(!$role){
            throw new \Exception('No role found');
        }
        foreach ($role->permissions as $permission){
            $user->withdrawTeamPermissionsTo([$permission->slug], $team->id);
        }
        $user->withdrawTeamRoles([$role->slug], $team->id);
        $team->team_members()->detach($user);
        $user->notify(new RemovesTeamMember([auth()->user(), $team]));
        $team->addActivity('left', 'from');
    }

    public function notifyTeamMembers($team)
    {
//        dd($team->team_members->toArray());
        foreach ($team->team_members as $member){
            //dont notify the auth user
            if ($member != auth()->user()){
                $member->notify(new DeletesTeam([auth()->user(), $team]));
            }
        }

    }
}
