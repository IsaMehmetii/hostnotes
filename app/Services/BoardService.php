<?php

namespace App\Services;

use App\Models\Board;
use App\Notifications\RemovesTeamMember;


class BoardService
{
    public function getBoardById($id)
    {
        return Board::findOrFail($id);
    }

    public function getAllBoards()
    {
        return Board::all();
    }

    public function getAllPublicBoards()
    {
        return Board::where('visibility', 'public')->where('closed', false)->get();
    }
    public function createBoard($request)
    {
        return Board::create($request->all());
    }

    public function checkBoardVisibility($board)
    {
        if ($board->hasVisibility('public')) {
            return response()->json($board);
        } elseif ($board->hasVisibility('team')) {
            $team = $board->team;
            if ($team->hasMember(auth()->user()) or $board->hasMember(auth()->user())) {
                return response()->json($board);
            }
        } elseif ($board->hasVisibility('private')) {
            if ($board->hasMember(auth()->user())) {
                return response()->json($board);
            }
        } elseif ($board->hasVisibility('organization')) {
            if ($board->hasMember(auth()->user())) {
                return response()->json($board);
            }

        }
        throw new \Exception('Unauthorized.', '403');
    }

    public function ifExists($array)
    {
        if ($array == '[]'){
            throw new \Exception('No record was found.');
        }
    }

    public function exception($message)
    {
        throw new \Exception('\''.$message.'\'');
    }

    public function detachMemberFromBoard($user, $board)
    {
        if(!$board->hasMember($user)){
                BoardService::exception('Member doesn\'t exist');
            }
        $board->board_members()->detach($user);
        $user->notify(new RemovesTeamMember([auth()->user(), $board]));

    }
}
