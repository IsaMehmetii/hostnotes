<?php
namespace App\Traits;

trait BoardTrait
{
    public function hasVisibility($visibility)
    {
        return $this->visibility == $visibility;
    }

    public function hasMember($user)
    {
        return $this->board_members()->newPivotStatementForId($user->id)->exists();
    }
    public function hasTeamMember($user)
    {
        return  $this->team->team_members->contains($user->id);
    }
    public function hasAdmin($user)
    {
        return $this->board_members->where('pivot.user_id', $user->id)->contains('pivot.role', 'admin');
    }
    public function hasUser($user)
    {
        return $this->board_members->where('pivot.user_id', $user->id)->contains('pivot.role', 'user');
    }

    public function addActivity($action, $addition = null)
    {
        //action list : added, archived, edited, joined, removed, invited, left
        //addition : from..., to...
        $this->activities()->create([
            'action' => $action,
            'addition' => $addition,
            'user_id' => auth()->user()->id
        ]);
        return $this;
    }

    public function isClosed()
    {
        return $this->closed == true;
    }
}
