<?php
namespace App\Traits\Permissions;

use App\Models\Team_permission as Permission;
use App\Models\Team_role as Role;

trait HasPermissionTrait
{
    public function giveTeamPermissionsTo($permissions, $team_id) {

        $permissions = $this->getAllTeamPermissions($permissions);

        if($permissions === null) {
            return $this;
        }
        foreach ($permissions as $permission){
            $this->team_permissions()->attach($permission, ['team_id' => $team_id]);
        }
        return $this;
    }

    public function withdrawTeamPermissionsTo($permissions) {

        $permissions = $this->getAllTeamPermissions($permissions);
        $this->team_permissions()->detach($permissions);
        return $this;

    }

    public function refreshTeamPermissions($permissions, $team_id) {

        $this->team_permissions()->detach();
        return $this->giveTeamPermissionsTo($permissions, $team_id);
    }

    public function hasTeamPermissionTo($permission, $team_id) {

        $permission = Permission::where('slug',$permission)->first();
        //if this permission doesnt exist return false
        if(!$permission){
            return false;
        }

//        dd('sad');
        return $this->hasTeamPermissionThroughRole($permission, $team_id) || $this->hasTeamPermission($permission, $team_id);
    }

    public function hasTeamPermissionThroughRole($permission, $team_id) {

        foreach ($permission->roles as $role){
            if ($this->team_roles->contains('slug', $role) &&
                $this->team_roles->contains('pivot.team_id', $team_id)) {
                return true;
            }
        }
        return false;
    }

    public function hasTeamRole($roles, $team_id) {

        foreach ($roles as $role) {
            if ($this->team_roles->contains('slug', $role) &&
                $this->team_roles->contains('pivot.team_id', $team_id)) {
                return true;
            }
        }
        return false;
    }

    public function Roles() {

        return $this->belongsToMany(Role::class,'user_team_roles');

    }
    public function Permissions() {

        return $this->belongsToMany(Permission::class,'user_team_permissions');

    }
    protected function hasTeamPermission($permission, $team_id) {
        return (bool) $this->team_permissions->where('slug', $permission->slug)->where('pivot.team_id', $team_id)->count();
    }

    protected function getAllTeamPermissions(array $permissions) {

        return Permission::whereIn('slug',$permissions)->get();

    }

//    public function checkUserTeam($user_roles_permissions, $team_id)
//    {
//        $check = true;
//        foreach ($user_roles_permissions as  $role_permission){
//            if($role_permission->pivot->team_id == $team_id){
//                $check = false;
//            }
//        }
//        if ($check){
//            return false;
//        }
//        return true;
//    }

}
