<?php
namespace App\Traits;
use App\Models\Team_activity as Activity;

trait TeamActivitiesTrait
{
    public function activities()
    {
        return $this->hasMany('App\Models\Team_activities');
    }

    public function addActivity($action, $addition = null)
    {
        //action list : added, archived, edited, joined, removed, invited, left
        //addition : from..., to...
        $this->activities()->create([
            'action' => $action,
            'addition' => $addition,
            'user_id' => auth()->user()->id
        ]);
        return $this;
    }
}
