<?php
namespace App\Traits\Roles;

use App\Models\Team_permission as Permission;
use App\Models\Team_role as Role;

trait HasTeamRolesTrait
{
    public function giveTeamRolesTo($roles, $team_id) {

        $roles = $this->getAllTeamRoles($roles);

        if($roles === null) {
            return $this;
        }
        foreach ($roles as $role){
            $this->team_roles()->attach($role, ['team_id' => $team_id]);
        }
        return $this;
    }
    public function withdrawTeamRoles($roles) {

        $roles = $this->getAllTeamRoles($roles);
        $this->team_roles()->detach($roles);
        return $this;

    }

    protected function getAllTeamRoles(array $roles) {

        return Role::whereIn('slug',$roles)->get();

    }

}
