<?php
namespace App\Traits;

trait TeamTrait
{
    public function hasMember($user)
    {
        return $this->team_members()->newPivotStatementForId($user->id)->exists();
    }

}
