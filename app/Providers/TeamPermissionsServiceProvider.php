<?php

namespace App\Providers;

use App\Models\Team_permission as Permission;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class TeamPermissionsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
//        try {
//            Permission::get()->map(function ($permission, $team_id) {
//
//                Gate::define($permission->slug, function ($user) use ($permission, $team_id) {
//                    dd($team_id);
//                    return $user->hasTeamPermissionTo($permission->slug, $team_id);
//                });
//            });
//        } catch (\Exception $e) {
//            report($e);
//            return false;
//        }
//
//        //Blade directives
//        Blade::directive('role', function ($role, $team_id) {
//            return "if(auth()->check() && auth()->user()->hasTeamRole([{$role}], {$team_id})) :"; //return this if statement inside php tag
//        });
//
//        Blade::directive('endrole', function ($role) {
//            return "endif;"; //return this endif statement inside php tag
//        });

    }
}
