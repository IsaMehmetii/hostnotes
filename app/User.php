<?php

namespace App;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\Permissions\HasPermissionTrait;
//use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\Roles\HasTeamRolesTrait;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasPermissionTrait, HasTeamRolesTrait;

    protected $guard_name = 'api';

    protected $fillable = [
        'name', 'email', 'password', 'lastname',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function detail()
    {
        return $this->hasOne('App\Models\User_detail');
    }
    public function setting()
    {
        return $this->hasOne('App\Models\User_setting');
    }
    public function activity()
    {
        return $this->hasOne('App\Models\User_activity');
    }
    public function session()
    {
        return $this->hasOne('App\Models\User_session');
    }
    public function team_members()
    {
        return $this->belongsToMany(
            'App\Models\Team',
            'team_members',
            'user_id',
            'team_id');
    }
    public function team_roles()
    {
        return $this->belongsToMany(
            'App\Models\Team_role',
            'user_team_roles',
            'user_id',
            'role_id')
            ->withPivot(['team_id']);
    }
    public function team_permissions()
    {
        return $this->belongsToMany(
            'App\Models\Team_permission',
            'user_team_permissions',
            'user_id',
            'permission_id')
            ->withPivot(['team_id']);
    }
    public function team_activities()
    {
        return $this->hasMany('App\Models\Team_activity');
    }
    public function board_members()
    {
        return $this->belongsToMany(
            'App\Models\Board',
            'board_members',
            'user_id',
            'board_id')->withPivot(['role']);
    }
    public function boards_favorite()
    {
        return $this->belongsToMany(
            'App\Models\Board',
            'boards_favorite',
            'user_id',
            'board_id');
    }
    public function boards_watched()
    {
        return $this->belongsToMany(
            'App\Models\Board',
            'user_boards_watched',
            'user_id',
            'board_id');
    }
    public function board_activities()
    {
        return $this->hasMany('App\Models\Board_activity');
    }
    public function board_lists_watched()
    {
        return $this->belongsToMany(
            'App\Models\Board',
            'board_list_watches',
            'user_id',
            'list_id');
    }




}
