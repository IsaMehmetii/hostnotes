<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBoardSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('board_settings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('board_id');
            $table->boolean('team_members_can_edit_join')->default(true);
            $table->foreign('board_id')->on('boards')->references('id')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('board_settings');
    }
}
