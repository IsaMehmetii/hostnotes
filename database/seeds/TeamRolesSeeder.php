<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class TeamRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = array('Admin', 'Editor', 'User');
        foreach ($roles as $role){
            DB::table('team_roles')->insert([
                'name' => $role,
                'slug' => strtolower($role),
            ]);
        }
    }
}
