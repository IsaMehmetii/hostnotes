<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//         $this->call(TeamTypeSeeder::class);
//         $this->call(TeamRolesSeeder::class);
//         $this->call(TeamPermissionsSeeder::class);
         $this->call(UsersTableSeeder::class);
    }
}
