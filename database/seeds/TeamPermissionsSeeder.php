<?php

use App\Models\Team_role;
use Illuminate\Database\Seeder;
use App\Models\Team_permission as Permission;

class TeamPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permission1 = Permission::create([
            'name' => 'remove member',
            'slug' => 'remove-member',
        ]);
        $permission2 = Permission::create([
            'name' => 'edit team',
            'slug' => 'edit-team',
        ]);
        $permission3 = Permission::create([
            'name' => 'view team',
            'slug' => 'view-team',
        ]);
        $permission4 = Permission::create([
            'name' => 'delete team',
            'slug' => 'delete-team',
        ]);

        Team_role::find(1)->permissions()->saveMany([$permission1, $permission2, $permission3, $permission4]);
        Team_role::find(2)->permissions()->saveMany([$permission1, $permission2, $permission3]);
        Team_role::find(3)->permissions()->saveMany([$permission3]);


    }
}

//        $permission1->roles()->attach('1');
//        $permission1->roles()->attach('2');
//        $permission1->roles()->attach('3');
//        $permission2->roles()->attach('2');
//        $permission2->roles()->attach('3');
//        $permission3->roles()->attach('3');
