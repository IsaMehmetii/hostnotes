<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = array(
            'Human Resources',
            'Marketing',
            'Education',
            'Engineering-IT',
            'Sales CRM',
            'Operations',
            'Small Business',
            'Other');

        foreach ($types as $type) {
            DB::table('team_types')->insert([
                'type' => $type
            ]);
        }
    }
}
