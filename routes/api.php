<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => 'api'], function ($router) {
    Route::get('/', function () {
        return response()->json('Welcome to hostNotes');
    });
    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');

    Route::group(['middleware' => 'auth'], function () {

        //user routes
        Route::group(['prefix' => 'user/'], function () {

            Route::post('me', 'AuthController@me');
            Route::post('refresh', 'AuthController@refresh');
            Route::post('logout', 'AuthController@logout');

            Route::patch('details', 'UserController@details');
            Route::get('details', 'UserController@detailsIndex');
            Route::patch('settings', 'UserController@settings');
            Route::get('settings', 'UserController@settingsIndex');
            Route::post('image', 'UserController@postProfileImage');
            Route::get('image', 'UserController@getProfileImage');
            Route::get('session', 'UserController@sessionIndex');
            Route::group(['prefix' => 'notifications/'], function () {
                Route::get('/', 'UserNotificationsController@index');
                Route::get('/read', 'UserNotificationsController@readNotifications');
                Route::get('/unread', 'UserNotificationsController@unreadNotifications');
                Route::get('/count', 'UserNotificationsController@countUnread');
                Route::get('/show/{id}', 'UserNotificationsController@show');
                Route::get('/r/{id}', 'UserNotificationsController@showAndMarkRead');
                Route::patch('/markread/all', 'UserNotificationsController@markAllAsRead');
                Route::patch('/markread/{id}', 'UserNotificationsController@markAsReadById');
                Route::patch('/markunread/{id}', 'UserNotificationsController@markAsUnReadById');

            });
        });

        Route::group(['prefix' => 'team/'], function () {

            Route::get('/types', 'TeamController@types');
            Route::post('/', 'TeamController@store');


            Route::get('/roles', 'TeamRolePermissionsController@roleIndex');
            Route::get('/permissions', 'TeamRolePermissionsController@permissionIndex');

            Route::get('/memberteams', 'TeamMembersController@memberIndex');
            Route::get('/{id}/members', 'TeamMembersController@teamMembers');
            Route::get('/{id}/members/count', 'TeamMembersController@teamMemberCount');
            Route::patch('/{id}/member', 'TeamMembersController@inviteTeamMembers'); //team member/s req->user_id= []
            //post
            Route::patch('/{id}/leave', 'TeamMembersController@memberLeave');
            //delete
            Route::patch('/{team_id}/remove/{user_id}', 'TeamMembersController@memberRemove');
            //delete
            Route::get('{id}/activities', 'TeamActivitiesController@activitiesIndex');
            Route::get('{team_id}/activity/{activity_id}', 'TeamActivitiesController@activitiesShow');

            Route::get('/visible', 'TeamController@visibleTeamsIndex');

            Route::get('/{id}', 'TeamController@show');
            Route::patch('/{id}', 'TeamController@update');
            Route::delete('/{id}', 'TeamController@destroy');

            Route::patch('/{id}/settings', 'TeamController@settingsUpdate');

            Route::get('/{id}/boards', 'TeamController@teamBoards');
        });

        Route::group(['prefix' => 'board/'], function () {
            Route::post('/', 'BoardController@store');
            Route::get('/', 'BoardController@getUserBoards');
            Route::post('/public', 'BoardController@publicBoards');
            Route::get('/favorite', 'BoardsFavoriteController@index');
            Route::get('/closed', 'BoardController@closed');

            //can-see-board users can see board
            Route::group(['middleware' => 'can-see-board'], function () {
                //favorite boards
                Route::group(['prefix' => 'favorite/'], function () {
                    Route::post('/{id}', 'BoardsFavoriteController@store');
                    Route::delete('/{id}', 'BoardsFavoriteController@destroy');
                });
                Route::get('/{id}/members/count', 'BoardMembersController@boardMembersCount');
                Route::get('/{id}/members', 'BoardMembersController@boardMembers');
                //user Board Watch
                Route::post('/{id}/watch', 'UserBoardWatchedController@watchBoard');
                Route::delete('/{id}/watch/remove', 'UserBoardWatchedController@removeWatchBoard');
                Route::get('/{id}/show', 'BoardController@show');
                Route::get('/{id}/visibility', 'BoardController@visibility');

                Route::get('/{id}/activity', 'BoardActivitiesController@index');
                Route::get('/{id}/activity/{activity_id}', 'BoardActivitiesController@show');

                //join board only boards that are team-membzers
                Route::post('/{id}/member/join', 'BoardMembersController@joinBoard');

                    //admins can change all settingsboard
                Route::group(['middleware' => 'board-member'], function () {

                    Route::post('/{id}/member/invite', 'BoardMembersController@inviteBoardMember');
                    Route::delete('/{id}/member/leave', 'BoardMembersController@boardMemberLeave');

                    //lists
                    Route::group(['prefix' => '{id}/list'], function () {
                        Route::get('/', 'BoardListController@index');
                        Route::get('/{list_id}', 'BoardListController@show');
                        Route::post('/store', 'BoardListController@store');
                        Route::patch('/update/{list_id}', 'BoardListController@update');
                        Route::patch('/delete/{list_id}', 'BoardListController@destroy');
                        Route::patch('/archive/{list_id}', 'BoardListController@archive');
                        Route::patch('/restore/{list_id}', 'BoardListController@restore');
                        Route::post('/watch/{list_id}', 'BoardListWatchedController@watchList');
                        Route::delete('/unwatch/{list_id}', 'BoardListWatchedController@removeWatchList');

                        Route::group(['prefix' => '{list_id}/task'], function () {
                            Route::get('/', 'BoardTaskController@index');
                            Route::get('/count', 'BoardTaskController@count');
                            Route::get('/{task_id}', 'BoardTaskController@show');
                            Route::post('/store', 'BoardTaskController@store');
                        });
                    });
                    //cards
                    Route::group(['middleware' => 'board-admin'], function () {
                        Route::patch('/{id}/team/change', 'BoardController@changeTeam');
                        Route::patch('/{id}/visibility/edit', 'BoardController@editVisibility');
                        Route::patch('/{id}/rename', 'BoardController@renameBoard');
                        Route::delete('/{id}/remove/{user_id}', 'BoardMembersController@memberRemove');
                        Route::delete('/{id}/delete', 'BoardController@destroy');
                        Route::patch('/{id}/close', 'BoardController@close');
                        Route::patch('/{id}/reopen', 'BoardController@reopen');
                        Route::patch('/{id}/move', 'BoardController@move');


                    });

                });
            });
        });

        Route::get('/test', 'TeamController@test');
    });


});



